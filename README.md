# mt-server

Trouble shooting
----------------

If *exercise_histories.begin_at* is not set to Asia/Seoul, 

you have to set mysql *global.time_zone* and *session.time_zone* as Asia/Seoul 

you can check your timezone with following command on mysql 

```bash
mysql> use mysql
mysql> select @@global.time_zone, @@session.time_zone;
```

you can check the time_zone is set as SYSTEM (default)

you have to change it to 'Asia/Seoul' by running following line


```bash
mysql> set global time_zone='Asia/Seoul';
```

if you see following error, you have to click [Here](https://dev.mysql.com/downloads/timezones.html) and download timezone zip file 

ERROR 1298 (HY000): Unknown or incorrect time zone: 'Asia/Seoul' 

You have to unzip timezone zip file and select all sql command in sql file 

and then copy and paste on mysql database of mysql

finally, you can run following command without error

```bash
mysql> set global time_zone='Asia/Seoul';
```

check your mysql time zone with following command

```bash
mysql> use mysql
mysql> select @@global.time_zone, @@session.time_zone;
```


| @@global.time_zone | @@session.time_zone |
| ------ | ------ |
| Asia/Seoul | Asia/Seoul |

you're all fixed to go









