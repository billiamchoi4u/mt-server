class ChangeAnswerTable < ActiveRecord::Migration[5.2]
  def self.down
    remove_column :answers, :report_id, :bigint
  end

  def change
    remove_column :answers, :no, :integer
    add_reference :answers, :exercise_history
    change_column :answers, :question_type, :string
  end
end
