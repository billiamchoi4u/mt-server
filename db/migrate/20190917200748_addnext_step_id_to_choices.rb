class AddnextStepIdToChoices < ActiveRecord::Migration[5.2]
  def change
    add_column :choices, :next_step_id, :bigint, default: 0
  end
end
