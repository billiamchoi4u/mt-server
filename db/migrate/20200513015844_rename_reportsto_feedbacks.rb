class RenameReportstoFeedbacks < ActiveRecord::Migration[5.2]
  def self.up
    rename_table :reports, :feedbacks
  end 

  def self.down
    rename_table :feedbacks, :reports
  end
end
