class CreateEpisodes < ActiveRecord::Migration[5.2]
  def change
    create_table :episodes do |t|
      t.integer :no
      t.string :title

      t.timestamps
    end
  end
end
