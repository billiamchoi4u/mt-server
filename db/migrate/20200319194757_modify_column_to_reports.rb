class ModifyColumnToReports < ActiveRecord::Migration[5.2]
  def change
    add_reference :reports, :day_history, Index: true
    add_column :reports, :title, :string
    add_column :reports, :target_day_seq, :string      
  end
end
