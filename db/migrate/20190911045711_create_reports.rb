class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.references :user, foreign_key: true
      t.references :exercise, foreign_key: true
      t.text :response
      t.datetime :responsed_at

      t.timestamps
    end
  end
end
