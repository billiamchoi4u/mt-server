class CreateAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :answers do |t|
      t.references :report, foreign_key: true
      t.integer :question_type, default: 0
      t.string :question_title
      t.text :question_value
      t.text :value
      t.integer :no
      
      t.timestamps
    end
  end
end
