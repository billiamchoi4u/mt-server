class AddCurrentEpisodeNoColumnToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :current_episode_no, :integer      
  end
end
