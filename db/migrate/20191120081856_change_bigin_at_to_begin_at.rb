class ChangeBiginAtToBeginAt < ActiveRecord::Migration[5.2]
  def change
    rename_column :exercise_histories, :bigin_at, :begin_at
  end
end
