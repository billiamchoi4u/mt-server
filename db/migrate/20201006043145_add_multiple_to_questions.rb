class AddMultipleToQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :questions, :multiple, :boolean, default: false
  end
end
