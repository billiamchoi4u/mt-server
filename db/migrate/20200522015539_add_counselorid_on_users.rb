class AddCounseloridOnUsers < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :counselor, index: true
  end
end
