class RemoveExerciseIdDayHistoryId < ActiveRecord::Migration[5.2]
  def change
    remove_column :feedbacks, :day_history_id
    remove_column :feedbacks, :exercise_id
  end
end
