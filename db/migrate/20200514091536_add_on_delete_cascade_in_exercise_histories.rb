class AddOnDeleteCascadeInExerciseHistories < ActiveRecord::Migration[5.2]
  def change
    remove_foreign_key "exercise_histories", "day_histories"
    add_foreign_key "exercise_histories", "day_histories", on_delete: :cascade
  end
end
