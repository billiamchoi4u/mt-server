class ChangeIndexExercisesOnNoToExercises < ActiveRecord::Migration[5.2]
  def change
    remove_index :exercises, :no
    add_index :exercises, :no
  end
end
