class CreateDayHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :day_histories do |t|
      t.references :user, foreign_key: true
      t.integer :day_seq
      t.integer :last_episode_no
      t.integer :last_exercise_no
      t.integer :total_exercise_no
      t.datetime :duration
      t.datetime :rdate
      t.timestamps
    end
  end
end
