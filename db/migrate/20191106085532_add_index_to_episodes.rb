class AddIndexToEpisodes < ActiveRecord::Migration[5.2]
  def change
    add_index :episodes, :no, unique: true
  end
end
