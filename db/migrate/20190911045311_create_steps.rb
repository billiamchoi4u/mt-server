class CreateSteps < ActiveRecord::Migration[5.2]
  def change
    create_table :steps do |t|
      t.integer :no
      t.integer :step_type
      t.string :title
      t.references :exercise, foreign_key: true
      t.text :description
      t.string :action_name

      t.timestamps
    end
  end
end
