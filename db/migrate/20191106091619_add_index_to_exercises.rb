class AddIndexToExercises < ActiveRecord::Migration[5.2]
  def change
    add_index :exercises, :no, unique: true
  end
end
