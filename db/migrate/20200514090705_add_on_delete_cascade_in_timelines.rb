class AddOnDeleteCascadeInTimelines < ActiveRecord::Migration[5.2]
  def change
    remove_foreign_key "timelines", "day_histories"
    remove_foreign_key "timelines", "feedbacks"
    add_foreign_key "timelines", "day_histories", on_delete: :cascade
    add_foreign_key "timelines", "feedbacks", on_delete: :cascade
  end
end
