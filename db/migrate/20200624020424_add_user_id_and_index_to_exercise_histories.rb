class AddUserIdAndIndexToExerciseHistories < ActiveRecord::Migration[5.2]
  def change
    add_reference :exercise_histories, :user, index: true
    ExerciseHistory.all.each do |e|
      e.user_id = e.day_history.user_id
      e.save
    end
  end
end
