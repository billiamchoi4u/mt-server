class ChangeDurationToBeStringInDayHistories < ActiveRecord::Migration[5.2]
  def change
    change_column :day_histories, :duration, :string
  end
end
