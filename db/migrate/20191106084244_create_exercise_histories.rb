class CreateExerciseHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :exercise_histories do |t|
      t.references :day_history, foreign_key: true
      t.references :exercise, foreign_key: true
      t.datetime :bigin_at
      t.datetime :end_at

      t.timestamps
    end
  end
end
