class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.integer :no
      t.string :title
      t.text :value
      t.integer :question_type, default: 0
      t.references :step, foreign_key: true

      t.timestamps
    end
  end
end
