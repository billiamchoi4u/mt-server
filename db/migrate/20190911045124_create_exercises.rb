class CreateExercises < ActiveRecord::Migration[5.2]
  def change
    create_table :exercises do |t|
      t.integer :no
      t.string :title
      t.integer :time
      t.references :episode, foreign_key: true
      t.timestamps
    end
  end
end
