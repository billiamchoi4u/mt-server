class CreateTimelines < ActiveRecord::Migration[5.2]
  def change
    create_table :timelines do |t|
      t.references :user, foreign_key: true
      t.integer :timeline_type, default: 0
      t.date :rdate
      t.integer :no, default: 0
      t.references :feedback, foreign_key: true
      t.references :day_history, foreign_key: true

      t.timestamps
    end
  end
end
