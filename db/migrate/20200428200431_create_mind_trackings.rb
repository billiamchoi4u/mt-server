class CreateMindTrackings < ActiveRecord::Migration[5.2]
  def change
    create_table :mind_trackings do |t|
      t.references :user, foreign_key: true
      t.integer :answer_1
      t.integer :answer_2
      t.integer :answer_3
      t.integer :answer_4
      t.integer :answer_5
      t.datetime :rdate
      t.timestamps
    end
  end
end
