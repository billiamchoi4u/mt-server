class AddNoToAnswers < ActiveRecord::Migration[5.2]
  def change
    add_column :answers, :no, :integer
  end
end
