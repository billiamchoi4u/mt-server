class RenameAnswerNumberToAnswernumber < ActiveRecord::Migration[5.2]
  def change
    rename_column :mind_trackings, :answer_1, :answer1
    rename_column :mind_trackings, :answer_2, :answer2
    rename_column :mind_trackings, :answer_3, :answer3
    rename_column :mind_trackings, :answer_4, :answer4
    rename_column :mind_trackings, :answer_5, :answer5
  end
end
