class ChangeDurationToBeIntegerInDayHistories < ActiveRecord::Migration[5.2]
  def change
    change_column :day_histories, :duration, :integer
  end
end
