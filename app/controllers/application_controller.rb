class ApplicationController < ActionController::Base
  before_action :set_host_for_local_storage, :authenticate_user!
  serialization_scope :view_context
  
  protected
  
  def set_host_for_local_storage
    ActiveStorage::Current.host = request.base_url
  end
end
