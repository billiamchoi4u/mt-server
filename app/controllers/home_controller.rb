class HomeController < ApplicationController
  skip_before_action :authenticate_user!, :except => [:index, :help]
  def index 
  end

  def help
  end
end
