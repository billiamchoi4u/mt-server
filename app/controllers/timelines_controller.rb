class TimelinesController < ApplicationController
  # add_breadcrumb "Users", :users_path

  before_action :set_timeline, only: [:show, :edit, :update, :destroy]
  before_action :set_user, only: [:show, :index, :edit, :create, :new, :update]
  before_action :set_mindtracking, only: [:index]
  # before_action :set_breadcrumb
  # GET /timelines
  # GET /timelines.json
  def index
    add_breadcrumb 'timelines'
    @timelines = Timeline.where(:user => @user).order(:no)
    @timeline = Timeline.where(:user => @user).where(:timeline_type => "day_history").last
    @ex_histo_exist = ExerciseHistory.where(:user => @user).where('end_at is not null')
  end

  # GET /timelines/1
  # GET /timelines/1.json
  def show
  end

  # GET /timelines/new
  def new
    @timeline = Timeline.new
    @timeline.build_feedback
    @timeline_length = Timeline.where(:user => @user).order(:no).length+1
  end

  # GET /timelines/1/edit
  def edit
  end

  # POST /timelines
  # POST /timelines.json
  def create
    @timeline = Timeline.new(timeline_params)
    respond_to do |format|
      if @timeline.save
        format.html { redirect_to  user_timelines_path(@user), notice: 'Timeline was successfully created.' }
        format.json { render :show, status: :created, location: @timeline }
      else
        format.html { render :new }
        format.json { render json: @timeline.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /timelines/1
  # PATCH/PUT /timelines/1.json
  def update
    respond_to do |format|
      if @timeline.update(timeline_params)
        format.html { redirect_to user_timelines_path(@user), notice: 'Timeline was successfully updated.' }
        format.json { render :show, status: :ok, location: @timeline }
      else
        format.html { render :edit }
        format.json { render json: @timeline.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /timelines/1
  # DELETE /timelines/1.json
  def destroy
    @user = @timeline.user
    @timeline.destroy
    respond_to do |format|
      format.html { redirect_to user_timelines_path(@user), notice: 'Timeline was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_user
      @user = User.find(params[:user_id])
    end  

    # def set_breadcrumb
    #   add_breadcrumb @user.email, user_timelines_path(@user) if @user.present?
    # end

    # Use callbacks to share common setup or constraints between actions.
    def set_timeline
      @timeline = Timeline.find(params[:id])
    end

    def set_mindtracking
      @mind_trackings = MindTracking.all.where(:user_id => @user.id).order(:rdate)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def timeline_params
      params.require(:timeline).permit(:user_id, :timeline_type, :rdate, :no, :day_history_id, feedback_attributes: [:response, :responsed_at, :user_id, :title, :target_day_seq])
    end
end
