class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :promoteToCounselor, :promoteToNormal, :down, :destroy]
  # before_action :set_breadcrumb
  # GET /users
  # GET /users.json
  def index
    if params[:user_type].present?
      @users = User.where(:user_type => params[:user_type]).order(user_type: :desc).order(name: :asc).page(params[:page]).per(10)
    else
      @users = User.where('user_type != 2').order(user_type: :desc).order(name: :asc).page(params[:page]).per(10)
    end
    
    @ex_histo_exist = ExerciseHistory.where(:user => @user).where('end_at is not null')
    @my_users = User.where(:counselor_id => current_user.id).order(name: :asc).page params[:page]
    @timeline = Timeline.where(:user => @user).where(:timeline_type => "day_history").last
    @user_type = params[:user_type]
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @clients = User.where(:counselor_id => @user.id)
  end

  # GET /users/new
  def new
    @user = @exercise.users.new
  end

  # GET /users/1/edit
  def edit
    add_breadcrumb "Edit"
  end

  # POST /users
  # POST /users.json
  def create
    @user = @exercise.users.new(user_params)
    @user.no = (@exercise.users.maximum('no') || 0) + 1

    respond_to do |format|
      if @user.save
        format.html { redirect_to exercise_users_path(@exercise), notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    # @exercise = @users.exercise
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to users_path, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def promoteToCounselor
    if @user.user_type == "normal"
      User.transaction do 
        @user.user_type = 1
        @user.save
      end
    end
    redirect_to users_path
  end

  def promoteToNormal
    if @user.user_type == "counselor"
      User.transaction do 
        @user.user_type = 0
        @user.save
      end
    end
    redirect_to users_path
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_path, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def auth
    @users = User.where("user_type = 0").order(name: :asc).page(params[:page]).per(10)
  end 

  def assign_counselor
    @users = User.where("user_type = 0").order(name: :asc).page(params[:page]).per(10)
    @counselors = User.where("user_type = 1")
  end

  def group_assign_counselor
    @user_ids_str = params[:hidden]
    @counselor_id = params[:counselor_id]
    if @user_ids_str.present?
      for user_id in @user_ids_str.split(",")
        @user = User.find(user_id)
        @user.counselor_id = @counselor_id
        @user.save
      end
    end
    redirect_to users_path
  end

  def group_auth
    @user_ids_str = params[:hidden]
    if @user_ids_str.present?
      for user_id in @user_ids_str.split(",")
        @user = User.find(user_id)
        @user.user_type = 1
        @user.save
      end
    end
    redirect_to users_path
  end




  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    def set_exercise
      @exercise = Exercise.find(params[:exercise_id])
    end  

    # def set_breadcrumb
    #   add_breadcrumb "Users", users_path
    # end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:no, :user_type, :counselor_id, :name, :nickname, :student_no, :birth_year)
    end
end
