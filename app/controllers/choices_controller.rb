class ChoicesController < ApplicationController
  before_action :set_choice, only: [:show, :edit, :update, :destroy]
  before_action :set_question, only: [:index, :new, :create]
  
  # GET /choices
  # GET /choices.json
  def index
    @choices = @question.choices
  end

  # GET /choices/1
  # GET /choices/1.json
  def show
  end

  # GET /choices/new
  def new
    @choice = @question.choices.new
    prepare_step_options
  end

  # GET /choices/1/edit
  def edit
    prepare_step_options
  end

  # POST /choices
  # POST /choices.json
  def create
    @choice = @question.choices.new(choice_params)

    respond_to do |format|
      if @choice.save
        format.html { redirect_to @choice, notice: 'Choice was successfully created.' }
        format.json { render :show, status: :created, location: @choice }
      else
        format.html { render :new }
        format.json { render json: @choice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /choices/1
  # PATCH/PUT /choices/1.json
  def update
    respond_to do |format|
      if @choice.update(choice_params)
        format.html { redirect_to @choice, notice: 'Choice was successfully updated.' }
        format.json { render :show, status: :ok, location: @choice }
      else
        format.html { render :edit }
        format.json { render json: @choice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /choices/1
  # DELETE /choices/1.json
  def destroy
    @choice.destroy
    respond_to do |format|
      format.html { redirect_to choices_url, notice: 'Choice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_choice
      @choice = Choice.find(params[:id])
      @question = @choice.question
    end
    def set_question
      @question = Question.find(params[:question_id])
    end
    def prepare_step_options
      current_step = @choice.question.step
      @step_options = current_step.exercise.steps
        .where('id != ?', current_step.id).all
        .map{|r| [r.title, r.id]}
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def choice_params
      params.require(:choice).permit(:no, :value, :question_id, :next_step_id)
    end
end
