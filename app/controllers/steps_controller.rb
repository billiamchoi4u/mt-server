class StepsController < ApplicationController
  add_breadcrumb "Episodes", :episodes_path
  before_action :set_step, only: [:show, :edit, :update, :destroy, :up, :down]
  before_action :set_exercise, only: [:index, :create, :new]
  before_action :set_breadcrumb
  # GET /steps
  # GET /steps.json
  def index
    @steps = @exercise.steps 
  end

  # GET /steps/1
  # GET /steps/1.json
  def show
  end

  # GET /steps/new
  def new
    @step = @exercise.steps.new
  end

  # GET /steps/1/edit
  def edit
    add_breadcrumb "Edit"
  end

  # POST /steps
  # POST /steps.json
  def create
    @step = @exercise.steps.new(step_params)
    @step.no = (@exercise.steps.maximum('no') || 0) + 1

    respond_to do |format|
      if @step.save
        format.html { redirect_to exercise_steps_path(@exercise), notice: 'Step was successfully created.' }
        format.json { render :show, status: :created, location: @step }
      else
        format.html { render :new }
        format.json { render json: @step.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /steps/1
  # PATCH/PUT /steps/1.json
  def update
    # @exercise = @steps.exercise
    respond_to do |format|
      if @step.update(step_params)
        format.html { redirect_to exercise_steps_path(@exercise), notice: 'Step was successfully updated.' }
        format.json { render :show, status: :ok, location: @step }
      else
        format.html { render :edit }
        format.json { render json: @step.errors, status: :unprocessable_entity }
      end
    end
  end

  def up
    if @step.no > 1
      Exercise.transaction do 
        @exercise.steps.where('no = ?', @step.no - 1).update_all("no = no + 1")
        @step.no = @step.no - 1
        @step.save
      end
    end
    redirect_to exercise_steps_path(@exercise)
  end
  
  def down
    if @step.no < @exercise.steps.size
      Step.transaction do 
        @exercise.steps.where('no = ?', @step.no + 1).update_all("no = no - 1")
        @step.no = @step.no + 1
        @step.save
      end
    end
    redirect_to exercise_steps_path(@exercise)
  end

  # DELETE /steps/1
  # DELETE /steps/1.json
  def destroy
    Exercise.transaction do 
      @exercise.steps.where('no > ?', @step.no).update_all("no = no - 1")
      @step.destroy
    end
    respond_to do |format|
      format.html { redirect_to exercise_steps_path(@exercise), notice: 'Step was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_step
      @step = Step.find(params[:id])
    end

    def set_exercise
      @exercise = Exercise.find(params[:exercise_id])
    end  

    def set_breadcrumb
      @exercise = @step.exercise if @exercise.blank? && @step.present? && @step.exercise.present?
      @episode = @exercise.episode if @episode.blank? && @exercise.present? && @exercise.episode.present?
      add_breadcrumb @episode.title, episode_exercises_path(@episode) if @episode.present?
      add_breadcrumb @exercise.title, exercise_steps_path(@exercise) if @exercise.present?
      add_breadcrumb @step.title, @step if @step.present?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def step_params
      params.require(:step).permit(:no, :step_type, :title, :exercise_id, :description, :action_name, :audio, :video, :cover)
    end
end
