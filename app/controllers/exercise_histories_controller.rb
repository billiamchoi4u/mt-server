class ExerciseHistoriesController < ApplicationController

  before_action :set_exercise_history, only: [:show, :edit, :update, :destroy]
  def index
    @exercise_histories = ExerciseHistory.all
  end

  def show
    @user = @exercise_history.day_history.user
    @timeline = @exercise_history.day_history.timeline
  end

  def edit
  end

  def set_exercise_history
    @exercise_history = ExerciseHistory.find(params[:id])
  end
end
