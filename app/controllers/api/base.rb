module API
  class Base < Grape::API
    # ...
    # 
    # require 'grape_logging'
    # logger.formatter = GrapeLogging::Formatters::Default.new
    # use GrapeLogging::Middleware::RequestLogger, { logger: logger }

    mount API::V1::RootV1
    def self.respond_to_error(e)
      logger.error e unless Rails.env.test? # Breaks tests...
      eclass = e.class.to_s
      Rails.logger.debug(eclass.inspect)
      message = "OAuth error: #{e}" if eclass =~ /WineBouncer::Errors/
      opts = { error: message || e.message }
      opts[:trace] = e.backtrace[0, 10] unless Rails.env.production?
      Rack::Response.new(opts.to_json, status_code_for(e, eclass), {
                           'Content-Type' => 'application/json',
                           'Access-Control-Allow-Origin' => '*',
                           'Access-Control-Request-Method' => '*'
                         }).finish
    end

    def self.status_code_for(error, eclass)
      Rails.logger.debug("ERROR NAME !!! #{eclass}")
      Rails.logger.debug("#{error}")
      Rails.logger.debug("#{error.message}")
      Rails.logger.debug("#{error.inspect}")
      if eclass =~ /OAuthUnauthorizedError/
        401
      elsif eclass =~ /OAuthForbiddenError/
        403
      elsif (eclass =~ /RecordNotFound/) || (error.message =~ /unable to find/i)
        404
      else
        (error.respond_to? :status) && error.status || 500
      end
    end
  end
end