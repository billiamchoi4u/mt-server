module API
  module V1
    class Day_histories< Grape::API
      include API::V1::Defaults

      resource :day_histories do
        oauth2
        desc 'Update day_histories'
        params do
          optional :day_seq, type: Integer, desc: 'day check number'
          optional :last_episode_no, type: Integer, desc: "last episode number"
          optional :last_exercise_no, type: Integer, desc: "last exercise number"
          optional :total_exercise_no, type: Integer, desc: "total exercise number"
          optional :duration, type: Integer, desc: "duration for exercise"
        end 
        put '/:id' do
          DayHistory.find_by_id(params[:id]).update({day_seq:params[:day_seq]})
          DayHistory.find_by_id(params[:id]).update({last_episode_no:params[:last_episode_no]})
          DayHistory.find_by_id(params[:id]).update({last_exercise_no:params[:last_exercise_no]})
          DayHistory.find_by_id(params[:id]).update({total_exercise_no:params[:total_exercise_no]})
          DayHistory.find_by_id(params[:id]).update({duration:params[:duration]})
        end
        oauth2
        get '/me' do
          DayHistory.where(:user_id => current_user.id).last
        end

   
        desc 'update day_seq when user appear on app',
        
          http_codes: [
            { code: 201, message: 'Success' },
            { code: 400, message: 'Error' }
          ]
        oauth2  
        # after login
        post 'first_touch' do
          begin
            @last_day_history = DayHistory.where(:user_id => current_user.id).last
            if @last_day_history.rdate.to_date < Time.current.to_date
              @new_day_history = DayHistory.new 
              @new_day_history.user_id = @last_day_history.user_id
              @new_day_history.day_seq = @last_day_history.day_seq + 1
              @new_day_history.last_episode_no = @last_day_history.last_episode_no
              @new_day_history.last_exercise_no =  @last_day_history.last_exercise_no
              @new_day_history.total_exercise_no =  @last_day_history.total_exercise_no
              @new_day_history.rdate = Time.now.in_time_zone('Seoul').to_date
              @new_day_history.duration = 0
              @new_day_history.save!
              @new_timeline = Timeline.new
              @new_timeline.user_id = current_user.id
              @new_timeline.timeline_type = 0
              @new_timeline.rdate = Time.current
              @new_timeline.no = Timeline.where(:user_id => current_user.id).length+1
              @new_timeline.day_history_id = @new_day_history.id
              @new_timeline.save!
            end
          end
        end     
      end  
        desc 'checking current user by day',
          http_codes: [
            { code: 201, message: 'Success' },
            { code: 400, message: 'Error' }
          ]
        params do
          requires :user_id, type: Integer, desc: 'id of user'
          requires :day_seq, type: Integer, desc: 'day check number'
          optional :last_episode_no, type: Integer, desc: "last episode number"
          optional :last_exercise_no, type: Integer, desc: "last exercise number"
          optional :total_exercise_no, type: Integer, desc: "total exercise number"
          optional :duration, type: Integer, desc: "duration for exercise"
        end
        post 'check' do
          begin
            @day_history = DayHistory.new
            @day_history.user_id = params[:user_id]
            @day_history.day_seq = params[:day_seq]
            @day_history.last_episode_no = params[:last_episode_no]
            @day_history.last_exercise_no = params[:last_exercise_no]
            @day_history.total_exercise_no = params[:total_exercise_no]
            @day_history.duration = params[:duration]
            @day_history.rdate = Date.today
            @day_history.save!
          end
        end     
      rescue_from ActiveRecord::RecordInvalid do |error|
        error!({code: 101, details: error.record.errors.details}, 400)
      end
    end
  end
end 
 
