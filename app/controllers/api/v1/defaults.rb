module API
  module V1
    module Defaults
      # if you're using Grape outside of Rails, you'll have to use Module#included hook
      extend ActiveSupport::Concern

      

      included do
        before do
          header['Access-Control-Allow-Origin'] = '*'
          header['Access-Control-Request-Method'] = '*'
          ActiveStorage::Current.host = request.base_url 
        end

        helpers do
          def current_token
            doorkeeper_access_token
          end

          def current_user
            resource_owner
          end

          def current_scopes
            current_token.scopes
          end
          
        end
      end    
    end
  end
end