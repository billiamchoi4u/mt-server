module API
  module V1
    class Exercise_histories< Grape::API
      include API::V1::Defaults

      resource :exercise_histories do
        desc 'create duration begin_at',
        http_codes: [
          { code: 201, message: 'Success' },
          { code: 400, message: 'Error' }
        ]
      params do
        requires :exercise_id, type: Integer, desc: 'id of exercise'
      end
        oauth2 
        post 'start' do 
          begin
            @last_day_history = DayHistory.where(:user_id => current_user.id).last
            @exercise_history = ExerciseHistory.new
            @exercise_history.user_id = current_user.id
            @exercise_history.day_history_id = @last_day_history.id
            @exercise_history.exercise_id = params[:exercise_id] 
            @exercise_history.begin_at = Time.current
            @exercise_history.save!
          end
        end

        desc 'create duration end_at',
        http_codes: [
          { code: 201, message: 'Success' },
          { code: 400, message: 'Error' }
        ]
        oauth2
        post 'end' do
          begin 
            @last_day_history = DayHistory.where(:user_id => current_user.id).last
            @exercise_history = ExerciseHistory.where(:day_history_id => @last_day_history.id).last
            @exercise_history.end_at = Time.current
            @exercise_history.user_id = current_user.id
            @exercise_history.save!

            if (@exercise_history.end_at - @exercise_history.begin_at) <= 60
              @last_day_history.duration += 1
            else 
              @last_day_history.duration += ((@exercise_history.end_at - @exercise_history.begin_at)/60).round
            end
            
            if @last_day_history.last_episode_no < @exercise_history.exercise.episode.no
              @last_day_history.last_episode_no = @exercise_history.exercise.episode.no
            end

            if @last_day_history.last_episode_no <= @exercise_history.exercise.episode.no 
              @last_day_history.last_exercise_no = ExerciseHistory.joins(:day_history, exercise: [:episode]).where("day_histories.user_id = ? and exercise_histories.end_at IS NOT NULL and episodes.no = ?", current_user.id, @exercise_history.exercise.episode.no).select("episodes.*, exercise_histories.*").pluck("DISTINCT exercise_id").length 
              @last_day_history.total_exercise_no = @exercise_history.exercise.episode.exercises.length
            end

            
            @last_day_history.save!

            if current_user.current_episode_no < 9          
              if ExerciseHistory.joins(:day_history, exercise: [:episode]).where("day_histories.user_id = ? and exercise_histories.end_at IS NOT NULL and episodes.no = ?", current_user.id, current_user.current_episode_no).select("episodes.*, exercise_histories.*").pluck("DISTINCT exercise_id").length == Episode.where(:no => current_user.current_episode_no).joins(:exercises).length
                User.find_by_id(current_user.id).update({current_episode_no: current_user.current_episode_no+1}) 
              end
            end
          end
        end

        oauth2
        desc 'Get All Exercise_Histories'
        get '/' do
          ExerciseHistory.all
        end
        
        

        desc 'Get last Exercise_Histories'
        params do
          optional :id, type: Integer, desc: "Exercise id"
        end 
        get '/last/:id' do
          ExerciseHistory.find_by_id(params[:id])
        end

        desc 'Update Exercise_Histories'
        params do
          optional :end_at, type: DateTime, desc: "exercise end time"
        end 

        put '/:id' do
          ExerciseHistory.find_by_id(params[:id]).update({end_at:params[:end_at]})
        end

        desc 'checking current user by day',
          http_codes: [
            { code: 201, message: 'Success' },
            { code: 400, message: 'Error' }
          ]
        params do
          requires :day_history_id, type: Integer, desc: 'id of day_history'
          requires :exercise_id, type: Integer, desc: 'exercise_id'
          optional :begin_at, type: DateTime, desc: 'exercise begin time'
          optional :end_at, type: DateTime, desc: "exercise end time"
        end
        post 'check' do
          begin
            @exercise_history = ExerciseHistory.new
            @exercise_history.day_history_id = params[:day_history_id]
            @exercise_history.exercise_id = params[:exercise_id]
            @exercise_history.begin_at = Time.current
            @exercise_history.save!
          end
        end
      end
    end
  end
end
