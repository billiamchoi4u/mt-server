module API
  module V1
    class Exercises < Grape::API
      include API::V1::Defaults
      require 'json'

      resource :exercises do
        desc 'This is for finding all exercises that is in a certain episode'
        oauth2
        get '/:id' do
          exercise = Exercise.where("episode_id = ?", params[:id]).order(no: :asc)
        end

        paginate per_page: 30, max_per_page: 100, offset: 0
        oauth2
        get '/' do
          Exercise.all
        end

        desc 'This is for finding all episode name for push-notification'
        get '/alarm/title' do
          Exercise.select(:id, :title).find_all
        end
      end

      rescue_from ActiveRecord::RecordInvalid do |error|
        error!({code: 101, details: error.record.errors.details}, 400)
      end
    end
  end
end