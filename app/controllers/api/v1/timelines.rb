module API
  module V1
    class Timelines < Grape::API
      include API::V1::Defaults

      resource :timelines do
                
      
        oauth2
        get '/' do
          Timeline.all.where(:user => current_user).order(:no)
        end

        desc 'This is for finding all mind trackings'
        oauth2
        get '/dayhistory' do
          Timeline.where(:user => current_user).where(:timeline_type => "day_history" ).order(:no).all
        end

        desc 'This is for finding all mind trackings'
        oauth2 
        get '/feedback' do
          Timeline.where(:user => current_user).where(:timeline_type => "feedback" ).order(:no).all
        end 
        
      end


      rescue_from ActiveRecord::RecordInvalid do |error|
        error!({code: 101, details: error.record.errors.details}, 400)
      end
    end
  end
end