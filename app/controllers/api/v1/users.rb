module API
  module V1
    class Users < Grape::API
    
      include API::V1::Defaults
    
      

      resource :users do
        desc 'user info for certain email'
        params do
          requires :email, type: String, desc: 'User Email'
        end
        get 'find' do
          user = User.where(email: params[:email])
        end

        desc 'get current user info'
        oauth2
        get 'me' do
          current_user
        end

        oauth2
        desc 'Update User Info'
        params do
          optional :name, type: String, desc: 'name of user'
          optional :nickname, type: String, desc: "nickname of user"
          optional :student_no, type: String, desc: "student_no of user"
          optional :birth_year, type: Integer, desc: "birth_year of user"
        end 
        put '/:id' do
          User.find_by_id(params[:id]).update({name:params[:name]})
          User.find_by_id(params[:id]).update({nickname:params[:nickname]})
          User.find_by_id(params[:id]).update({student_no:params[:student_no]})
          User.find_by_id(params[:id]).update({birth_year:params[:birth_year]})
          User.find_by_id(params[:id])
        end

      
        

        paginate per_page: 30, max_per_page: 100, offset: 0
        oauth2
        get '/' do
          users = User.all
          paginate(users)
        end
      end

      rescue_from ActiveRecord::RecordInvalid do |error|
        error!({code: 101, details: error.record.errors.details}, 400)
      end
    end
  end
end