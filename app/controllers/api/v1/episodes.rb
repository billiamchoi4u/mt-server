module API
  module V1
    class Episodes < Grape::API
      include API::V1::Defaults

      resource :episodes do
        desc 'This is for finding all exercises that is in a certain episode'
        oauth2 
        get '/:id' do
          episode = Episode.find(params[:id])
          result = episode.exercises.map do |e| 
            eh = ExerciseHistory.where(['exercise_id = ? AND user_id = ?', e.id, current_user.id]).first
            {exercise: e, completed: eh.present?}
          end
        end  

        paginate per_page: 30, max_per_page: 100, offset: 0
        oauth2
        get '/' do

          episodes = Episode.all
          paginate(episodes)
        end
      end

      rescue_from ActiveRecord::RecordInvalid do |error|
        error!({code: 101, details: error.record.errors.details}, 400)
      end
    end
  end
end