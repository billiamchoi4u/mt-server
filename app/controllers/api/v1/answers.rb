module API
  module V1
    class Answers < Grape::API
      include API::V1::Defaults

      resource :answers do
        desc "Get all the answers"
        oauth2 
        get '/' do
          Answer.all
    
        end

        desc "Get all the answers"
        oauth2 
        get '/group' do
          
          Answer.joins(:id).group_by(&:exercise_history_id)
    
        end
      

        desc 'Submit Answers'
        params do
          optional :no, type: Integer, desc: 'Number of Answers'
          optional :question_type, type: String, desc: 'Question_type'
          optional :question_title, type: String, desc: 'Question_title'
          optional :question_value, type: String, desc: 'Questionare'
          optional :value, type: String, desc: 'answer or file path'
        end

        oauth2
        post '/' do
          # begin
          @last_day_history = DayHistory.where(:user_id => current_user.id).last
          @exercise_history = ExerciseHistory.where(:day_history_id => @last_day_history.id).last
          answer = Answer.new
          answer.user_id = current_user.id
          answer.exercise_history_id = @exercise_history.id
          answer.no = params[:no]
          answer.question_type = params[:question_type]
          answer.question_title = params[:question_title]
          answer.question_value = params[:question_value]
          answer.value = params[:value]
          answer.save!
          answer
        end

      rescue_from ActiveRecord::RecordInvalid do |error|
        error!({code: 101, details: error.record.errors.details}, 400)
        end
      end
    end
  end
end
