module API
  module V1
    class Mind_trackings < Grape::API
      include API::V1::Defaults

      resource :mind_trackings do
        desc 'This is for finding all mind trackings'
        oauth2 
        get '/' do
          MindTracking.all
        end 

        desc 'This is for finding  mind trackings of current user'
        oauth2 
        get '/me' do
          current_user.mind_trackings
        end 
        
        desc 'Create mind trackings record'
        params do
          requires :rdate, type: String, desc: 'YYYY-MM-DD'
          optional :answer1, type: Integer, desc: 'answer number one'
          optional :answer2, type: Integer, desc: 'answer number two'
          optional :answer3, type: Integer, desc: 'answer number three'
          optional :answer4, type: Integer, desc: 'answer number four'
          optional :answer5, type: Integer, desc: 'answer number five'
        end

        oauth2
        post '/' do
          # begin
          mind_tracking = MindTracking.where(user_id: current_user.id, rdate: params[:rdate]).first_or_initialize
          mind_tracking.answer1 = params[:answer1]
          mind_tracking.answer2 = params[:answer2]
          mind_tracking.answer3 = params[:answer3]
          mind_tracking.answer4 = params[:answer4]
          mind_tracking.answer5 = params[:answer5]
          mind_tracking.save!
          mind_tracking
        end

        oauth2
        desc 'Update mind trackings record'
        params do
          requires :rdate, type: String, desc: 'YYYY-MM-DD'
          optional :answer1, type: Integer, desc: 'answer number one'
          optional :answer2, type: Integer, desc: 'answer number two'
          optional :answer3, type: Integer, desc: 'answer number three'
          optional :answer4, type: Integer, desc: 'answer number four'
          optional :answer5, type: Integer, desc: 'answer number five'
        end 
        put '/:id' do
          MindTracking.find_by_id(params[:id]).update({rdate:params[:rdate]})
          MindTracking.find_by_id(params[:id]).update({answer1:params[:answer1]})
          MindTracking.find_by_id(params[:id]).update({answer2:params[:answer2]})
          MindTracking.find_by_id(params[:id]).update({answer3:params[:answer3]})
          MindTracking.find_by_id(params[:id]).update({answer4:params[:answer4]})
          MindTracking.find_by_id(params[:id]).update({answer5:params[:answer5]})
          MindTracking.find_by_id(params[:id])
        end
      end

      rescue_from ActiveRecord::RecordInvalid do |error|
        error!({code: 101, details: error.record.errors.details}, 400)
      end
    end
  end
end
