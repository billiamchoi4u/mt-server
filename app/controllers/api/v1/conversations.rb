module API
  module V1
    class Conversations < Grape::API
      include API::V1::Defaults

      resource :conversations do
        desc 'Get all Conversation of Current User'
        oauth2
        get '/' do
          Conversation.where(:user_id => current_user.id).all
        end

        desc 'Create Question'
        params do
          optional :title, type: String, desc: 'Title of Question'
          optional :question, type: String, desc: 'Context of Question'
        end

        oauth2
        post '/' do
          conversation = Conversation.new
          conversation.user_id = current_user.id
          conversation.title = params[:title]
          conversation.question = params[:question]
          conversation.save!
          conversation
        end

        
      end

      rescue_from ActiveRecord::RecordInvalid do |error|
        error!({code: 101, details: error.record.errors.details}, 400)
      end
    end
  end
end
