module API
  module V1
    class Auth < Grape::API
      include API::V1::Defaults
      
      resource :auth do
        desc '가입',
          http_codes: [
            { code: 201, message: 'Success' },
            { code: 400, message: 'Error' }
          ]
        params do
          requires :email, type: String, desc: 'Email address'
          requires :password, type: String, desc: 'Password'
          requires :name, type: String, desc: 'Name'
          optional :nickname, type: String, desc: 'Nickname'
          optional :student_no, type: String, desc: 'Student No'
          requires :gender, type: Integer, desc: '성별 남=0, 여=1'
          optional :birth_year, type: Integer, desc: '생년 (예: 1998)'
        end
        post 'register' do
          begin
            @user = User.new
            @user.email = params[:email]
            @user.password = params[:password]
            @user.nickname = params[:nickname]
            @user.name = params[:name]
            @user.student_no = params[:student_no]
            @user.gender = params[:gender]
            @user.birth_year = params[:birth_year]
            @user.current_episode_no = 1
            @user.save!
            @first_day_history = DayHistory.new 
            @first_day_history.user_id = @user.id
            @first_day_history.day_seq = 1
            @first_day_history.last_episode_no = 1
            @first_day_history.rdate = Time.current
            puts Time.current.inspect
            @first_day_history.duration = 0
            @first_day_history.save!
            @first_timeline = Timeline.new
            @first_timeline.user_id = @user.id
            @first_timeline.timeline_type = 0
            @first_timeline.rdate = Time.current
            @first_timeline.no = 1
            @first_timeline.day_history_id = @first_day_history.id
            @first_timeline.save!
          rescue => e 
            Rails.logger.debug(e.inspect)
            # Rollbar.error(e)
            return error!({code: 399, error: e}, 400)
          end
          token = Doorkeeper::AccessToken.create!(
            application_id: nil,
            resource_owner_id: @user.id,
            expires_in: 100.days,
            scopes: 'public',
            use_refresh_token: true
          )
          {
            token: {
              'access_token'  => token.token,
              'token_type'    => token.token_type,
              'expires_in'    => token.expires_in_seconds,
              'refresh_token' => token.refresh_token,
              'scope'         => token.scopes_string,
              'created_at'    => token.created_at.to_i
            }.reject { |_, value| value.blank? }, 
            user: MeSerializer.new(@user).as_json
          }
        end
        desc 'Change password'
        params do
          requires :email, type: String, desc: 'Email address'
          requires :password, type: String, desc: 'Password'
        end
        post 'change_password' do
          user = User.where(email: params[:email]).first 
          return error!({code: 404, message: 'Not exist email'}, 404) if user.blank?
          if user.nil?
            return { exist: false} 
          else 
            user.password =  params[:password]
            user.save!
          end
        end
        desc 'Reset password'
        params do
          requires :email, type: String, desc: 'Email address'
        end
        post 'reset_password' do
          user = User.where(email: params[:email]).first
          return error!({code: 404, message: 'Not exist email'}, 404) if user.blank?
          user.send_reset_password_instructions
          {ret: "sent", email: user.email}
        end
        desc 'check if an email address exists',
          http_codes: [
            {code: 200, message: 'OK'}
          ]
        params do
          requires :email, type: String, desc: 'Email address'
        end 
        get 'exist_email' do
          user = User.where("email = ?", params[:email]).first
          if user.nil?
            return { exist: false}
          else
            return { exist: true}
          end  
        end
      end    
      rescue_from ActiveRecord::RecordInvalid do |error|
        error!({code: 101, details: error.record.errors.details}, 400)
      end
    end
  end
end
