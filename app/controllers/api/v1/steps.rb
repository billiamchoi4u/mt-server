module API
  module V1
    class Steps < Grape::API
      include API::V1::Defaults

      resource :steps do
        paginate per_page: 30, max_per_page: 100, offset: 0    
        desc 'This is for finding all steps that is in a certain exercise'
        get '/:id' do
          Step.where("exercise_id = ?", params[:id]).order(no: :asc)
        end

        get '/' do
          Step.where(:title => "프로그램 소개")
        end
      end

      rescue_from ActiveRecord::RecordInvalid do |error|
        error!({code: 101, details: error.record.errors.details}, 400)
      end
    end
  end
end