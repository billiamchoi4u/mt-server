require 'grape-swagger'
module API
  module V1
    class RootV1 < API::Base
      format :json
      formatter :json, Grape::Formatter::ActiveModelSerializers
      version 'v1'
      prefix 'api'
      default_error_formatter :json
      ActiveModelSerializers.config.default_includes = "**"
      content_type :json, 'application/json'
      use ::WineBouncer::OAuth2

      rescue_from ::WineBouncer::Errors::OAuthUnauthorizedError do |e|
        error!("#{e.message}", 401)
      end
      rescue_from ::WineBouncer::Errors::OAuthForbiddenError do |e|
        error!("#{e.message}", 403)
      end
      # rescue_from :grape_exceptions
      rescue_from :all do |e|
        API::Base.respond_to_error(e)
      end

      
      mount API::V1::Steps
      mount API::V1::Episodes
      mount API::V1::Exercises
      # mount API::V1::Configs
      # mount API::V1::Devices
      # mount API::V1::Gifts
      # mount API::V1::Scores
      # mount API::V1::Instructors
      # mount API::V1::Inventories
      # mount API::V1::Reservations
      # mount API::V1::Freetimes
      # mount API::V1::Phrases
      # mount API::V1::Products
      # mount API::V1::Notices
      # mount API::V1::Qnas
      # mount API::V1::Faqs
      mount API::V1::Auth
      # mount API::V1::Tunings
      # mount API::V1::Sales
      # mount API::V1::InstructorPays
      mount API::V1::Timelines
      mount API::V1::Users
      mount API::V1::Day_histories
      mount API::V1::Exercise_histories
      mount API::V1::Mind_trackings
      mount API::V1::Answers
      mount API::V1::Conversations

      add_swagger_documentation \
        endpoint_auth_wrapper: WineBouncer::OAuth2

      route :any, '*path' do
        raise StandardError, 'Unable to find endpoint'
      end
    end
  end
end
