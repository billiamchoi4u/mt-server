class ExercisesController < ApplicationController
  before_action :set_exercise, only: [:show, :edit, :update, :destroy, :up, :down]
  before_action :set_episode, only: [:index, :create, :new, :preview]
  # GET /exercises
  # GET /exercises.json
  def index
    @exercises = @episode.exercises
  end

  # GET /exercises/1
  # GET /exercises/1.json
  def show
  end

  # GET /exercises/new
  def new
    @exercise = @episode.exercises.new
    @exercise.time = 3
  end

  # GET /exercises/1/edit
  def edit
  end

  # POST /exercises
  # POST /exercises.json
  def create
    @exercise = @episode.exercises.new(exercise_params)
    @exercise.no = (@episode.exercises.maximum('no') || 0) + 1
    respond_to do |format|
      if @exercise.save
        format.html { redirect_to episode_exercises_path(@episode), notice: 'Exercise was successfully created.' }
        format.json { render :show, status: :created, location: @exercise }
      else
        format.html { render :new }
        format.json { render json: @exercise.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /exercises/1
  # PATCH/PUT /exercises/1.json
  def update
    # @episode = @exercises.episode
    respond_to do |format|
      if @exercise.update(exercise_params)
        format.html { redirect_to episode_exercises_path(@episode), notice: 'Exercise was successfully updated.' }
        format.json { render :show, status: :ok, location: @exercise }
      else
        format.html { render :edit }
        format.json { render json: @exercise.errors, status: :unprocessable_entity }
      end
    end
  end

  def up
    if @exercise.no > 1
      Exercise.transaction do 
        @episode.exercises.where('no = ?', @exercise.no - 1).update_all("no = no + 1")
        @exercise.no = @exercise.no - 1
        @exercise.save
      end
    end
    redirect_to episode_exercises_path(@episode)
  end
  
  def down
    if @exercise.no < @episode.exercises.size
      Exercise.transaction do 
        @episode.exercises.where('no = ?', @exercise.no + 1).update_all("no = no - 1")
        @exercise.no = @exercise.no + 1
        @exercise.save
      end
    end
    redirect_to episode_exercises_path(@episode)
  end

  # DELETE /exercises/1
  # DELETE /exercises/1.json
  def destroy
    Exercise.transaction do 
      @episode.exercises.where('no > ?', @exercise.no).update_all("no = no - 1")
      @exercise.destroy
    end
    respond_to do |format|
      format.html { redirect_to episode_exercises_path(@episode), notice: 'Exercise was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def preview
    @exercises = @episode.exercises
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_exercise
      age = 3
      @exercise = Exercise.find(params[:id])
      @episode = @exercise.episode
    end
    def set_episode
      @episode = Episode.find(params[:episode_id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def exercise_params
      params.require(:exercise).permit(:no, :title, :time, :episode_id)
    end
end
