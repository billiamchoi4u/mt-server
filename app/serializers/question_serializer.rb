class QuestionSerializer < ActiveModel::Serializer
  attributes :id, :no, :title, :value, :question_type, :placeholder, :multiple, :required
  has_many :choices
end
