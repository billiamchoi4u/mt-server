class EpisodeSerializer < ActiveModel::Serializer
  attributes :id, :no, :title
  has_many :exercises
end
