class MindTrackingSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :answer1, :answer2, :answer3, :answer4, :answer5
  attribute :rdate_str, key: :rdate
end

