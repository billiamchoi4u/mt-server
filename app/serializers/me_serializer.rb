class MeSerializer < ActiveModel::Serializer
  attributes :id, :email, :nickname, :birth_year, :gender, :student_no
end