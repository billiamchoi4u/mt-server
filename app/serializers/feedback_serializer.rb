class FeedbackSerializer < ActiveModel::Serializer
  attributes :id, :title, :target_day_seq, :user, :response, :responsed_at
end