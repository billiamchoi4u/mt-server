class DayHistorySerializer < ActiveModel::Serializer
  attributes :id, :user_id, :day_seq, :last_episode_no, :last_exercise_no, :total_exercise_no, :duration, :rdate
  has_many :exercise_history
end

