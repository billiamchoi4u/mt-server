class ExerciseHistorySerializer < ActiveModel::Serializer
  attributes :id, :day_history_id, :exercise_id, :exercise_title, :begin_at, :end_at
  has_many :answers

 

  def exercise_title
    object.exercise.title
  end
end
