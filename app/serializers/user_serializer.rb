class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :name, :nickname, :birth_year, :gender, :student_no, :user_type, :current_episode_no
  has_many :day_histories
end
