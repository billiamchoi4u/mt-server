class ExerciseSerializer < ActiveModel::Serializer
  attributes :id, :no, :title, :time, :completed
  has_many :steps
  
  def completed
    if scope.present? && scope.current_user.present?
      eh = ExerciseHistory.where([
            "exercise_id = ? and end_at IS NOT NULL and user_id = ?",
            self.object.id,
            scope.current_user.id
          ]).first
      return eh.present?   
    end
    return nil
  end

end
