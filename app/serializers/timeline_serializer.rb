class TimelineSerializer < ActiveModel::Serializer
  attributes :id, :timeline_type, :rdate, :no 
  has_one :user
  has_one :feedback
  has_one :day_history
end
