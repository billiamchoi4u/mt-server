class StepSerializer < ActiveModel::Serializer
  attributes :id, :no, :title, :step_type, :description, :action_name, :audio_url, :video_url, :cover_url
  has_many :questions, if: -> { object.question_required? }
end
