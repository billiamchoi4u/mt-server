class ChoiceSerializer < ActiveModel::Serializer
  attributes :id, :question_id, :no, :next_step_id, :value, :next_step_no
end