class AnswerSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :exercise_history, :exercise_title, :no, :question_type, :question_title, :question_value, :value

  def exercise_title
    object.exercise_history.exercise.title
  end
end