class ConversationSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :title, :question, :answer, :name
end
