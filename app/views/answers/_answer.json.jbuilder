json.extract! answer, :id, :report_id, :question_type, :question_title, :question_value, :value, :created_at, :updated_at
json.url answer_url(answer, format: :json)
