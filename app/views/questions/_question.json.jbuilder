json.extract! question, :id, :no, :title, :value, :step_id, :created_at, :updated_at
json.url question_url(question, format: :json)
