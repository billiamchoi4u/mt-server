json.extract! timeline, :id, :user_id, :timeline_type, :rdate, :no, :feedback_id, :day_history_id, :created_at, :updated_at
json.url timeline_url(timeline, format: :json)
