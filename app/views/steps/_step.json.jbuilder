json.extract! step, :id, :no, :step_type, :title, :exercise_id, :description, :action_name, :created_at, :updated_at
json.url step_url(step, format: :json)
