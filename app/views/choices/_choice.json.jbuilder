json.extract! choice, :id, :no, :value, :question_id, :created_at, :updated_at
json.url choice_url(choice, format: :json)
