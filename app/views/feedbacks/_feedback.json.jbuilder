json.extract! feedback, :id, :user_id, :exercise_id, :response, :responsed_at, :created_at, :updated_at
json.url feedback_url(feedback, format: :json)
