json.extract! conversation, :id, :title, :user_id, :question, :answer, :name, :created_at, :updated_at
json.url conversation_url(conversation, format: :json)
