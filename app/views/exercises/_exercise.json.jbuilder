json.extract! exercise, :id, :no, :title, :time, :episode_id, :created_at, :updated_at
json.url exercise_url(exercise, format: :json)
