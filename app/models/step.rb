class Step < ApplicationRecord
  belongs_to :exercise
  has_many :questions, -> { order(no: :asc)}, dependent: :destroy

  has_one_attached :audio
  has_one_attached :video
  has_one_attached :cover
  
  enum step_type: [
    :image_type, :image_full_type, :audio_type, :animation_type,
    :textbox_type, :question_type, :choice_type, :measure_type, 
    :record_type, :tombstone_type, :sandswamp_game_type, :water_tap_type, 
    :bubble_type, :pure_pain_type, :picker_type 
  ]

  def prev
    self.exercise.steps.where('no < ?', self.no).last
  end

  def next
    self.exercise.steps.where('no > ?', self.no).first
  end

  def first?
    self.prev.blank?
  end

  def last?
    self.next.blank?
  end

  def action_required?
    self.animation_type?
  end

  def video_required?
    self.animation_type?
  end

  def audio_url
    self.attach_url self.audio
  end
  
  def video_url
    self.attach_url self.video
  end

  def cover_url
    self.attach_url self.cover
  end

  def question_required?
    self.max_questions > 0
  end

  def max_questions
    return 1 if self.textbox_type? || self.choice_type? || self.record_type? || self.tombstone_type? || self.picker_type?
    return 3 if self.question_type? || self.measure_type?
    return 0
  end

  def question_registrable?
    self.questions.size < self.max_questions
  end
end
