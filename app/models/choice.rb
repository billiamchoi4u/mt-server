class Choice < ApplicationRecord
  belongs_to :question
  
  def prev
    self.question.choices.where('no < ?', self.no).last
  end

  def next
    self.question.choices.where('no > ?', self.no).first
  end

  def first?
    self.prev.blank?
  end

  def last?
    self.next.blank?
  end

  def next_step
    Step.where('id = ?', self.next_step_id).first
  end

  def default_next_step
    self.question.step.next
  end

  def next_step_no
    return self.next_step.no if self.next_step.present?
    return self.default_next_step.no if self.default_next_step.present?
  end
end
