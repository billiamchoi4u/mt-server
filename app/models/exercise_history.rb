class ExerciseHistory < ApplicationRecord
  belongs_to :day_history
  belongs_to :exercise
  has_many :answers, -> { order(no: :asc)}, dependent: :destroy

  def duration
    @minute_diff = ((self.end_at - self.begin_at) / 1.minutes).to_i
    if (@minute_diff > 1)
      return ((self.end_at - self.begin_at) / 1.minutes).to_i
    else return 1
    end
  end
end
