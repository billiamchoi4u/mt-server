class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  include Rails.application.routes.url_helpers
  
  def attach_url(blob)
    blob.service_url if blob.attached?
  end
end
