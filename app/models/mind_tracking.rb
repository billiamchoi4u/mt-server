class MindTracking < ApplicationRecord
  belongs_to :user

  def rdate_str
    self.rdate.strftime("%Y-%m-%d")
  end
end
