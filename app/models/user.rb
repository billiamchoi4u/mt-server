class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, 
         :trackable

  enum user_type: [:normal, :counselor, :admin]
  enum gender: [:male, :female]

  
  has_many :day_histories, dependent: :destroy
  has_many :answer, dependent: :destroy
  has_many :mind_trackings, -> { order(rdate: :asc) }, dependent: :destroy
  has_many :timelines, -> { order(no: :desc) }, dependent: :destroy
  
  def age
    Time.new.year - self.birth_year
  end
end
