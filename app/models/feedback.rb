class Feedback < ApplicationRecord
  belongs_to :user
  has_one :timeline, :inverse_of => :feedback
end
