class DayHistory < ApplicationRecord
  belongs_to :user
  has_many :exercise_history, dependent: :destroy 
  has_many :answers, through: :exercise_history
  has_one :timeline
end