class Timeline < ActiveRecord::Base
  enum timeline_type: [:day_history, :feedback]
  belongs_to :user
  belongs_to :day_history, optional: true, dependent: :destroy
  belongs_to :feedback, optional: true, :inverse_of => :timeline, dependent: :destroy
  accepts_nested_attributes_for :feedback, allow_destroy: true
end
