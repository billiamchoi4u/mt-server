class Exercise < ApplicationRecord
  belongs_to :episode
  has_many :steps, -> { order(no: :asc)}, dependent: :destroy
  has_many :exercise_histories, dependent: :destroy

  def prev
    Exercise.where('no < ?', self.no).last
  end

  def next
    Exercise.where("no > ?", self.no).first
  end

  def first?
    self.prev.blank?
  end

  def last?
    self.next.blank?
  end
end
