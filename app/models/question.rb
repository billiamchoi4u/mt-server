class Question < ApplicationRecord
  belongs_to :step
  has_many :choices, -> { order(no: :asc)}, dependent: :destroy
  enum question_type: [:long, :short, :choice, :measure, :record]

  def prev
    self.step.questions.where('no < ?', self.no).last
  end

  def next
    self.step.questions.where('no > ?', self.no).first
  end

  def first?
    self.prev.blank?
  end

  def last?
    self.next.blank?
  end


  def max_choices
    return 5 if self.step.choice_type?
    return 0
  end

  def choice_required?
    self.max_choices > 0
  end

  def question_registrable?
    self.choices.size < self.max_choices
  end
end
