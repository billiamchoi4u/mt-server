class Episode < ApplicationRecord
  has_many :exercises, -> { order(no: :asc)}, dependent: :destroy
end
