GrapeSwaggerRails.options.url      = '/api/v1/swagger_doc'
if Rails.env.development?
  GrapeSwaggerRails.options.app_url  = 'http://localhost:3000'
else
  GrapeSwaggerRails.options.app_url  = 'http://3.35.89.246/'
end
GrapeSwaggerRails.options.hide_url_input = true
GrapeSwaggerRails.options.app_name = 'MindTracen API'
GrapeSwaggerRails.options.api_auth     = 'bearer' # Or 'bearer' for OAuth
GrapeSwaggerRails.options.api_key_name = 'Authorization'
GrapeSwaggerRails.options.api_key_type = 'header'

GrapeSwaggerRails.options.before_action do |request|
  # 1. Inspect the `request` or access the Swagger UI controller via `self`.
  # 2. Check `current_user` or `can? :access, :api`, etc.
  # 3. Redirect or error in case of failure.
  if !user_signed_in?
    redirect_to "/users/sign_in", flash: {error: I18n.t("devise.failure.unauthenticated")}
  elsif !current_user.admin?
    redirect_to "/", flash: {error: "Not authorized"} 
  end
end
