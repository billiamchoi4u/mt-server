Rails.application.routes.draw do
  
  resources :conversations
  get 'home/help'
  use_doorkeeper
  resources :episodes do 
    resources :exercises
      get '/exercise_preview', to: 'exercises#preview'
  end

  get '/episodes_preview', to: 'episodes#preview'
  
  

  resources :choices
  resources :questions do 
    resources :choices
  end

  resources :exercises do 
    get 'up', on: :member
    get 'down', on: :member
    resources :steps 
  end  
  
  resources :steps do
    get 'up', on: :member
    get 'down', on: :member 
    resources :questions
  end
    
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  


  get 'home/index'
  get 'home/help'
  get 'home/privacy'

  resources :users do
      collection do
        get 'group_auth'
        get 'group_assign_counselor'
      end
    
    get 'promoteToCounselor', on: :member
    get 'promoteToNormal', on: :member
    resources :timelines do 
      resources :exercise_histories
    end 
  end
  
  get '/users_auth', to: 'users#auth'
  get '/assign_counselor', to: 'users#assign_counselor'




  resources :timelines
  resources :feedbacks
  resources :conversations
  
  

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # use_doorkeeper
  mount API::Base => '/'
  mount GrapeSwaggerRails::Engine =>  '/swagger_doc'
  
  root to: "users#index"
end
